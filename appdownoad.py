# -*- coding:utf-8 -*-


from ftplib import FTP
import json
import os
import time
import hashlib
import shutil
import sys
import urllib2
from smb.SMBConnection import SMBConnection


class ConnectSamba():

    def __init__(self, ip):
        SAMBA_USERNAME = "test"
        SAMBA_PASSWORD = "test"
        SAMBA_PORT = 139
        SAMBA_MYNAME = "test"
        SAMBA_REMOTEIP = ip
        SAMBA_DOMAINNAME = ""
        SAMBA_DIR = 'test'
        self.user_name = SAMBA_USERNAME
        self.pass_word = SAMBA_PASSWORD
        self.my_name = SAMBA_MYNAME
        self.domain_name = SAMBA_DOMAINNAME
        self.remote_smb_IP = SAMBA_REMOTEIP
        self.port = SAMBA_PORT
        self.dir = SAMBA_DIR

    def downloadFile(self, filename, download_filepath):
        print 'file path is : ' + download_filepath + filename
        try:
            conn = SMBConnection(self.user_name, self.pass_word, self.my_name, self.domain_name, use_ntlm_v2=False)
            conn.connect(self.remote_smb_IP, self.port)
            file_obj = open(filename, 'wb')
            print "Start download CIFS files ......"
            time1 = round(time.time() * 1000)
            conn.retrieveFile(self.dir, download_filepath + filename, file_obj)
            time2 = round(time.time() * 1000)
            file_obj.close()
            return int(time2) - int(time1)
        except BaseException, e:
            print 'error', e
            return 0

    def uploadFile(self, filename, upload_path):
        try:
            conn = SMBConnection(self.user_name, self.pass_word, self.my_name, self.domain_name, use_ntlm_v2=False)
            conn.connect(self.remote_smb_IP, self.port)

            file_obj = open(filename, 'rb')
            time1 = round(time.time() * 1000)
            conn.storeFile(self.dir, upload_path+filename, file_obj)
            time2 = round(time.time() * 1000)
            file_obj.close()
            return int(time2) - int(time1)
        except BaseException, e:
            print 'error', e
            return 0


def init_ftp(ip):
    ftp = FTP()
    try:
        ftp.set_debuglevel(2)
        ftp.connect(ip, "21")
        ftp.login("test", "test")
        print ftp.getwelcome()
        print 'Current Dir is : ' + ftp.pwd()
        return ftp
    except BaseException, e:
        print 'error', e
        return 0


def ftp_download(ftp, localpath, remotepath):
    bufsize = 1024
    if os.path.isfile(localpath):
        os.remove(localpath)
    try:
        fp = open(localpath, 'wb')
        print "Start download FTP files ......"
        time1 = round(time.time() * 1000)
        ftp.retrbinary('RETR ' + remotepath, fp.write, bufsize)
        time2 = round(time.time() * 1000)
        ftp.set_debuglevel(0)
        ftp.close()
        return int(time2) - int(time1)
    except BaseException, e:
        print 'error', e
        return 0

def calmd5(localpath):
    md5 = ''
    if os.path.isfile(localpath):
        m = hashlib.md5()
        f = open(localpath, 'rb')
        m.update(f.read())
        f.close()
        md5 = m.hexdigest()
        print m.hexdigest()
    return md5


def calsize(filepath):
    filesize = os.stat(filepath).st_size
    print 'file size is : ' + str(filesize) + " B       " + str(float(filesize) / 1024 / 1024) + " MB"
    return filesize


def calspeed(downloadtime, filesize):
    print 'download time is : ' + str(downloadtime) + 'ms'
    speed = float(filesize) / 1024 / 1024 / (float(downloadtime) / 1000)
    print "speed is : " + str(speed) + " MB      " + str(speed * 8) + " Mb"
    return round(speed, 3)

def http_download(ip,filename):
    if os.path.isfile(filename):
        os.remove(filename)
    print "Start downloading HTTP file ... ..."
    url = 'http://'+ip+'/sangfor/'+filename

    try:
        time1 = round(time.time() * 1000)
        f = urllib2.urlopen(url)
        data = f.read()
        with open(filename, "wb") as code:
            code.write(data)
        time2 = round(time.time() * 1000)
        return int(time2) - int(time1)
    except BaseException, e:
        print 'error', e
        return 0

def main(args):
    if not os.path.isfile('test_result.json'):
        shutil.copy('test_result-default.json','test_result.json')
    if len(args) < 2:
        sys.exit(1)
    ip=args[1]
    filelist = ["10M.zip"]
    fileresult = {}
    resultpath = "test_result.json"
    try:
        with open(resultpath, 'r') as load_f:
            fileresult = json.load(load_f)
    except BaseException, e:
        print "Error: Loading JSON file Faile , Error is : ", e
    else:
        print "Success: Loading JSON file Success !"

    for file in filelist:
        localpath = file
        fileresult["time"].append(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
        remotepath = "sangfor/" + file
        #### FTP下载
        ftp = init_ftp(ip)
        downloadtime = ftp_download(ftp, localpath, remotepath)
        if downloadtime != 0:
            filesize = calsize(localpath)
            fileresult["downloadavgspeed"].append(str(calspeed(downloadtime, filesize) * 8) + " Mb")
            fileresult["localpath"].append(localpath)
            fileresult["type"].append("ftp")
            fileresult["filesize"].append(str(round(float(filesize) / 1024 / 1024, 3)) + "MB")
            fileresult["filemd5"].append(calmd5(localpath))
            fileresult["downloadtime"].append(str(downloadtime) + 'ms')
            with open(resultpath, 'w') as f:
                json.dump(fileresult, f)
                print ("Result conf modify SUCCESS ,resultpath is : " + resultpath)

        #### CIFS下载
        fileresult["time"].append(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
        smbtest = ConnectSamba(ip)
        downloadtime = smbtest.downloadFile(localpath, "/sangfor/")
        if downloadtime != 0:
            filesize = calsize(localpath)
            fileresult["downloadavgspeed"].append(str(calspeed(downloadtime, filesize) * 8) + " Mb")
            fileresult["localpath"].append(localpath)
            fileresult["type"].append("cifs")
            fileresult["filesize"].append(str(round(float(filesize) / 1024 / 1024, 3)) + "MB")
            fileresult["filemd5"].append(calmd5(localpath))
            fileresult["downloadtime"].append(str(downloadtime) + 'ms')
            with open(resultpath, 'w') as f:
                json.dump(fileresult, f)
                print ("Result conf modify SUCCESS ,resultpath is : " + resultpath)
        else:
            print 'CIFS Download Fail ... ...'

        fileresult["time"].append(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
        downloadtime = http_download(ip,localpath)
        if downloadtime != 0:
            filesize = calsize(localpath)
            fileresult["downloadavgspeed"].append(str(calspeed(downloadtime, filesize) * 8) + " Mb")
            fileresult["localpath"].append(localpath)
            fileresult["type"].append("http")
            fileresult["filesize"].append(str(round(float(filesize) / 1024 / 1024, 3)) + "MB")
            fileresult["filemd5"].append(calmd5(localpath))
            fileresult["downloadtime"].append(str(downloadtime) + 'ms')
            with open(resultpath, 'w') as f:
                json.dump(fileresult, f)
                print ("Result conf modify SUCCESS ,resultpath is : " + resultpath)
        else:
            print 'HTTP Download Fail ... ...'



if __name__ == "__main__":
    argvs=sys.argv
    if len(argvs) == 3:
        i=0
        count=argvs[2]
        while i<int(count):
            print "Start download " + str(i) + " ... ..."
            main(argvs)
            i+=1
    if len(argvs) == 2:
        main(argvs)
